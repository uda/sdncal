/*
Ported from sdncal20.zip from https://www.scottlee.com/interests.html
Downloaded on 2021-11-23
*/

/* $selId: dow.c,v 2.0 1995/10/24 01:13:06 lees Exp $
 * Copyright 1993-1995, Scott E. Lee, all rights reserved.
 * Permission granted to use, copy, modify, distribute and sell so long as
 * the above copyright and this permission statement are retained in all
 * copies. THERE IS NO WARRANTY - USE AT YOUR OWN RISK.
 */

/**************************************************************************
 *
 * These are the externally visible components of this file:
 *
 *     DayOfWeek(sdn  int)  int
 *
 * Convert an SDN to a day-of-week number (0 to 6). Where 0 stands for
 * Sunday, 1 for Monday, etc. and 6 stands for Saturday.
 *
 *     DayNameShort  [7]string
 *
 * Convert a day-of-week number (0 to 6), as returned from DayOfWeek(), to
 * the abbreviated (three character) name of the day.
 *
 *     DayNameLong  [7]string
 *
 * Convert a day-of-week number (0 to 6), as returned from DayOfWeek(), to
 * the name of the day.
 *
 **************************************************************************/

package sdncal

var DayNameShort = [7]string{
	"Sun",
	"Mon",
	"Tue",
	"Wed",
	"Thu",
	"Fri",
	"Sat",
}

var DayNameLong = [7]string{
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
}

func DayOfWeek(sdn int) int {
	var dow int
	dow = (sdn + 1) % 7
	if dow >= 0 {
		return dow
	} else {
		return dow + 7
	}
}
