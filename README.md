# sdncal

This package contains the source code of Go language functions for date
arithmetic and for converting between the following calendar types:

* Gregorian
* Julian
* French Republican
* Jewish

There are general comments about using the software in the common file
(common.go) and specific comments on each calendar in its source code file
(e.g., julian.go).

If you find bugs, have suggestions for improvement, etc., Open a ticket :)

## Original README

This package contains the source code of C language functions for date
arithmetic and for converting between the following calendar types:

    Gregorian
    Julian
    French Republican
    Jewish

Other calendars may be supported in the future.  Let me know what other
calendars you would like to see added.

There are also verification programs for each calendar (file names starting
with "v").

There are general comments about using the software in the header file
(sdncal.h) and specific comments on each calendar in its source code file
(e.g., julian.c).

If you find bugs, have suggestions for improvement, etc., the author
welcomes your comments:

    Scott E. Lee
    scottlee@pobox.com
    (This email is no more relevant)
    CompuServe: (This detail is no more relevant)

If you cannot find me at the above addresses, try looking for me in the
Internet White Page Directory at:

    _This address no more shows intended content._
